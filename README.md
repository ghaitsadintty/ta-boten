**Nama Anggota Kelompok:**
1. Hilda Ayu Azizah Febyanti (215150401111060)
2. Anggrea Putri Rachmadhani (215150407111065)
3. Ghaitsa Tiara Dintty (215150407111066)

**Tentang "NoteKU"**

**NoteKU** adalah aplikasi catatan sederhana yang didesain untuk mempermudah user dalam mencatat dan mengelola catatan harian mereka. Aplikasi ini tersedia dengan tampilan UI yang user-friendly sehingga memberikan pengalaman yang intuitif bagi pengguna untuk mengelola catatan sehari-hari.

**Fitur Utama:**
1. Buat catatan\
Membuat catatan baru dengan mudah. Pengguna cukup menekan satu tombol untuk dapat mencatat ide, tugas, atau pikiran penting mereka.
2. Lihat catatan\
Akses catatan dengan cepat dan mudah melalui daftar catatan yang terorganisir. User dapat melihat catatan sebelumnya untuk referensi atau inspirasi.
3. Edit catatan\
Perbarui catatan yang ada sesuai kebutuhan. Dengan fitur ini, user dapat memperbaharui informasi atau menambahkan detail lebih lanjut ke catatan yang sudah ada.
4. Hapus Catatan\
Hapus catatan yang tidak diperlukan lagi. NoteKU memastikan pengguna memiliki kendali penuh atas catatan mereka dan dapat menghapusnya dengan mudah.
5. Cari Catatan\
Mencari catatan berdasarkan judul catatan. User memasukkan memasukkan judul yang akan dicari catatan pada kolom search. 


**Link PPT**: https://www.canva.com/design/DAF0AfGK8KI/hT7_lFg3yOvwI3Ft4Nm5XQ/edit?utm_content=DAF0AfGK8KI&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton 
