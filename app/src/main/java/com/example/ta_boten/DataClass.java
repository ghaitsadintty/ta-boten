package com.example.ta_boten;

public class DataClass {

    private String dataTopic;
    private String dataDescription;
    private String dataCategory;
    private String dataImage;
    private String key;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    public String getDataTopic() {
        return dataTopic;
    }

    public String getDataDescription() {
        return dataDescription;
    }

    public String getDataCategory() {
        return dataCategory;
    }

    public String getDataImage() {
        return dataImage;
    }

    public DataClass(String dataTopic, String dataDescription, String dataCategory, String dataImage) {
        this.dataTopic = dataTopic;
        this.dataDescription = dataDescription;
        this.dataCategory = dataCategory;
        this.dataImage = dataImage;
    }

    public DataClass(){

    }
}
